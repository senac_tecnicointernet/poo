<?php

abstract class Forma
{
	public $tipoDeForma = 'Forma Abstrata';
	
	public function imprimeForma()
	{
		$this -> calculaArea();
		echo $this -> tipoDeForma . ' com Área de: ' . $this -> calculaArea();
	}
	
	abstract public function calculaArea();
}

class Quadrado extends Forma
{
	public $lado;

	public function __construct(float $varLado)
	{
		$this -> tipoDeForma = ' Quadrado';
		$this -> lado = $varLado;
	}
	public function calculaArea()
	{
		return $this -> lado * $this -> lado;
	}
}

class Retangulo extends Forma
{
	public $base;
	public $altura;

	public function __construct(float $varBase, $varAltura)
	{
		$this -> tipoDeForma = 'Retângulo';
		$this -> base = $varBase;
		$this -> altura = $varAltura;
	}
	public function calculaArea()
	{
		return $this -> base * $this -> altura;
	}
}

class Triangulo extends Forma
{
	public $comprimentoBase;
	public $altura;

	public function __construct(float $comprimentoBase, $altura)
	{
		$this -> tipoDeForma = 'Triangulo';
		$this -> comprimentoBase = $comprimentoBase;
		$this -> altura = $altura;
	}
	public function calculaArea()
	{
		return ($this -> comprimentoBase * $this -> altura)/2;
	}
}

$obj = new Quadrado(5);
$obj -> imprimeForma();

echo "<br>";

$objl =  new Quadrado(100);
$objl -> imprimeForma();

echo "<br>";

$objRetangulo = new Retangulo(5,10);
$objRetangulo -> imprimeForma();

echo "<br>";

$objTriangulo = new Triangulo(2,5);
$objTriangulo -> imprimeForma();


?>