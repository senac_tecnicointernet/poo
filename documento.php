<?php

abstract class Documento
{
    protected $numero;

    abstract public function eValido();

    abstract public function formata();

    public function setNumero($numero)
    {
        $this->numero = preg_replace('/[^0-9]/', '', $numero);      //expressão regular.
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function calculaModulo11($qtdeNumeros, $peso)
    {
        $digito = 0;
        $somatorio = 0;
        $documento = substr($this->getNumero(), 0, $qtdeNumeros);

        for ($index = 0; $index < $qtdeNumeros; $index++) {

            $somatorio += $documento[$index] * $peso--;
        }

        if( $peso <2)
        {
            $peso = 9;
        }


        $modulo11 = $somatorio % 11;
        $digito = 11 - $modulo11;

        if ($digito > 9) {
            $digito = 0;
        }

        return $digito;

        echo "<br>";
    }
}

class CPF extends Documento
{

    public function __construct($numero)
    {

        $this->setNumero($numero);        //Usa a função setNumero

    }
    public function eValido()
    {

        $digitox = $this->calculaModulo11(9, 10);
        $digitoy = $this->calculaModulo11(10, 11);


        echo "Digito X = " . $digitox;
        echo "<br>";
        echo "Digito Y = " . $digitoy;
        echo "<br>";


        $cpfCalculado = substr($this->getNumero(), 0, 9);

        $cpfCalculado = $cpfCalculado . $digitox . $digitoy;

        if ($this->getNumero() == $cpfCalculado) {
            return true;
        } else {
            return false;
        }
    }

    public function formata()
    {
        //Formato do CPF: ###.###.###-##
        return substr($this->numero, 0, 3) . '.' .     //Retorna o atributo, no codigo para aparecer na tela, quando usado o protected
            substr($this->numero, 3, 3) . '.' .
            substr($this->numero, 6, 3) . '-' .
            substr($this->numero, 9, 2);
    }
};

class CNPJ extends Documento
{
    public function __construct($numero)
    {

        $this->setNumero($numero);
    }
    public function eValido()
    {
        $digitox = $this->calculaModulo11(12, 5);
        $digitoy = $this->calculaModulo11(13, 6);


        $cnpjCalculado = substr($this->getNumero(), 0, 9);
        $cnpjCalculado . $digitox . $digitoy;

      


        /*$digitox = 0;
        $somatoriox = 0;
        $cpfx = substr($this->getNumero(), 0, 12);
        $peso = 10;

        for ($index = 0; $index < 9; $index++) {
            $somatoriox += $cpfx[$index] * $peso--;
        }

        if ($digitox > 9) {
            $digitox = 0;
        }

        echo $somatoriox;

        $modulo11 = $somatoriox % 11;
        echo "</br>" . $modulo11;

        $digitox = 11 - $modulo11;
        echo "</br>" . $digitox;

        echo "</br>" . $digitox;
        echo "</br>";

        $somatorioy =  0;
        $cpfy = substr($this->getNumero(), 0, 10);
        $pesoy = 11;

        for ($index = 0; $index < 10; $index++) {
            $somatorioy += $cpfy[$index] * $pesoy--;
        }

        echo $somatorioy;

        $modulo11 = $somatorioy % 11;
        echo "</br>" . $modulo11;

        $digitoy = 11 - $modulo11;
        echo "</br>" . $digitoy;

        echo "</br>" . $digitoy;
        echo "</br>";

        if ($this->getNumero() == $cnpjCalculado) {
            return true;
        } else {
            return false;
        }*/
    }
    public function formata()
    {
    }
};

class CNH extends Documento
{
    private $categoria;

    public function __construct(string $numero, $categoria)
    {

        $this->setNumero($numero);
        $this->categoria = $categoria;
    }
    public function eValido()
    {
    }
    public function formata()
    {
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }
};

echo "CPF Paola";
echo "<br>";
$cpfPaola = new CPF("171.865.758-72");
if ($cpfPaola->eValido()) {
    echo "CPF Válido";
} else {
    echo "CPF Inválido";
};

echo "<br>";

$cnpjSenac = new CNPJ("03.709.814/0001-98");
if ($cnpjSenac->eValido()) {
    echo "CNPJ válido";
} else {
    echo "CNPJ inválido";
}

echo "<br>";








/*
$cpf = new CPF('123.456.789-00');
echo $cpf -> formata();


$cpf -> setNumero('123.456.789-09');
echo '<br>';
echo $cpf -> getNumero();

echo '<br>';

$cnpjSenac = new CNPJ('03.709.814/0025-65');
echo $cnpjSenac -> getNumero();

echo '<br>';

$cnhAlguem = new CNH('1255656', 'AB');
echo $cnhAlguem -> getNumero() . ' Cat.: ' . $cnhAlguem -> getCategoria();

/* $cpf -> numero = '123.456.789-00';
     echo '<br>';
    echo $cpf -> formata(); 
    
*/
