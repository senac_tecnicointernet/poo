<?php

abstract class Conta    //classe abstrata (pai)
{
    private $tipoDaConta;     //variaveis que armazenam os dados
    public function getTipoDaConta()
    {
        return $this -> tipoDaConta;
    }

    public function setTipoDaConta(string $tipoDaConta)
    {
        $this -> tipoDaConta = $tipoDaConta;
    }

    
    public $agencia;
    public $conta;
    protected $saldo;

    public function imprimeExtrato()
    {
        echo "TIPO: " . $this -> tipoDaConta;
        echo"<br>";
        echo "CONTA: " . $this -> conta ;
        echo"<br>";
        echo"AGÊNCIA: " . $this -> agencia ;
        echo"<br>";
        echo "SALDO: " . $this-> calculaSaldo();   #Para acessar todo membro da classe usamos o 'this ->'
    }

    public function deposito(float $valor)      //Valores que passam pelos parametros
    {
       if ($valor > 0 )     #Verifica se o valor que você passou como parametro é valido
       {
            echo "Deposito realizado com sucesso!";
            echo"<br>";
            $this -> saldo = $this -> saldo + $valor;
           
       }
       else
       {
            echo "Deposito Invalido";
            echo"<br>";
       }

    }

    public function saque(float $valor)      //Valores que passam pelos parametros
    {
        if ($this -> saldo >= $valor)
        {
            $this -> saldo -= $valor;  //$this -> saldo = $this -> saldo + $valor; --- 'São iguais'
            echo "Saque realizado com sucesso!";
            echo"<br>";
        } 
        else 
        {
            echo "Saldo insuficiente.";
            echo"<br>";
        }
       
    }

    abstract public function calculaSaldo();
}

  class Poupanca extends Conta
{
    public $reajuste;

    public function __construct( string $conta, $agencia, float $reajuste)
    {
        $this -> setTipoDaConta ('Poupança');
        $this -> agencia = $agencia;
        $this -> conta = $conta;
        $this -> reajuste = $reajuste;
    }

    public function calculaSaldo()
    {
        return $this -> saldo +($this -> saldo * $this -> reajuste / 100);
    }
}

class Especial extends Conta
{
    public $saldoEspecial;

    public function __construct( string $conta, $agencia, float $saldoEspecial)     //Parametros ou Variaveis Locais
    {
        $this -> setTipoDaConta ('Especial');
        $this -> agencia = $agencia;
        $this -> conta = $conta;
        $this -> reajuste = $saldoEspecial;
    }

    public function calculaSaldo()
    {
        return  $this -> saldo + $this -> saldoEspecial;
    }
}


$ctaPoupanca = new Poupanca ('00027-0', '85588-8', 0.54);   //Dados para os objetos, que passam pelos parametros determinados.
#$ctaPoupanca -> saldo = -1500;  //Não poode acessar atributo protegido
$ctaPoupanca -> deposito(1500);
$ctaPoupanca -> saque (30);
$ctaPoupanca -> imprimeExtrato();