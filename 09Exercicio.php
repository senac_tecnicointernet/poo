<?php

abstract class Conta
{
    public $tipoConta = "Void";
    public $agencia = 'void';
    public $conta = 'void';
    public $saldo = 0;

    public function imprimeExtrato()
    {
      
        echo "CONTA: " . $this -> conta ;
        echo"<br>";
        echo"AGÊNCIA: " . $this -> agencia ;
        echo"<br>";
        echo "SALDO: " . $this-> calculaSaldo();
    }


    public function deposito(float $valor)
    {
        $this -> saldo = $this -> saldo + $valor;
    }


    public function saque(float $valor)
    {
        $this -> saldo = $this -> saldo - $valor;
    }
   
    
    abstract public function calculaSaldo();
};

class Poupanca extends Conta
{
    public $reajuste;
    

    public function __construct( string $conta, $agencia, float $var_reajuste)
    {
        $this-> tipoConta = "Poupança";
        $this -> conta = $conta;
        $this -> agencia = $agencia;
        $this -> reajuste = $var_reajuste;
       
    }

     public function calculaSaldo()
     {
        return $this -> saldo +($this -> saldo * $this -> reajuste / 100);
     }
};

class Especial extends Conta
{
    public $saldoEspecial;

    public function __construct( string $conta, $agencia, float $var_saldoEspecial)
    {
        $this-> tipoConta = "Poupança";
        $this -> conta = $conta;
        $this -> agencia = $agencia;
        $this -> saldoEspecial = $var_saldoEspecial;
       
    }

     public function calculaSaldo()
     {
        return  $this -> saldo + $this -> saldoEspecial;
     }
};


$ctapoup = new Poupanca ('0002-7', '8558-88', 0.54);
$ctapoup -> deposito(1500);
$ctapoup -> imprimeExtrato();

echo"<hr>";

$ctaEspecial = new Especial ('0055-2', '7554-82', 2300);
$ctaEspecial-> deposito(1500);
$ctaEspecial -> imprimeExtrato();